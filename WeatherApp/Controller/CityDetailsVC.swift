//
//  CityDetailsVC.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/25/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import UIKit

protocol CityDetailsVCDelegate
{
    func dismissCityDetailsVC()
}

class CityDetailsVC: UIViewController
{
    // MARK: - IBOutlets
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var currentTemp: UILabel!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var windDegree: UILabel!
    
    // MARK: - iVars
    
    var delegate: CityDetailsVCDelegate?
    var city: City?
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.cityName.text = (self.city?.name)!
        self.descriptionLbl.text = (self.city?.condition?.description)!
        self.currentTemp.text = "\(Int((self.city?.condition?.currentTemperature)!))°"
        self.maxTemp.text = "\(Int((self.city?.condition?.maximumTemperature)!))°"
        self.minTemp.text = "\(Int((self.city?.condition?.minimumTemperature)!))°"
        self.humidity.text = "\((self.city?.condition?.humidity)!)%"
        self.pressure.text = "\((self.city?.condition?.pressure)!)"
        self.windSpeed.text = "\((self.city?.condition?.windSpeed)!)"
        self.windDegree.text = "\((self.city?.condition?.windDegree)!)"
    }
    
    // MARK: - Target Actions
    
    @IBAction func donePressed(_ sender: UIBarButtonItem)
    {
        if let delegate = self.delegate
        {
            delegate.dismissCityDetailsVC()
        }
    }
}
