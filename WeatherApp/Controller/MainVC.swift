//
//  MainVC.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/24/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import MBProgressHUD

let kEGYPTLAT = 26.8206
let kEGYPTLONG = 30.8025

class MainVC: UIViewController, MKMapViewDelegate, CityDetailsVCDelegate
{
    // MARK: - IBOutlets
    
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - iVars
    
    var citiesArray = [City]()
    var hudProgress: MBProgressHUD!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Add Annotation View for reuseability
        self.mapView.register(MKPinAnnotationView.self,
                              forAnnotationViewWithReuseIdentifier: "cityAnnotation")
        
        // Fetch all cities data and parse it
        self.fetchCities()
        
        // Initialize the progressView
        self.hudProgress = MBProgressHUD.showAdded(to: self.view,
                                                   animated: true)
        // Configre hudProgress
        self.hudProgress.mode = .indeterminate
        self.hudProgress.animationType = .fade
        self.hudProgress.label.text = "Loading Data..."
        
        // Request weather data for each city
        self.requestAllWeatherDataFromServer()
        
        // Setup MapView to be centered on Egypt
        self.setupMapView()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        // Hide NavBar
        self.navigationController?.setNavigationBarHidden(true,
                                                          animated: false)
    }
    
    // MARK: - MKMapViewDelegate Methods
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        let identifier = "cityAnnotation"
        
        let annotationView: MKPinAnnotationView!
        
        // Get an annotationView from mapView - the first time would be normally created but the second time would be dequeued -
        if let anno = self.mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        {
            annotationView = anno
        }
        else
        {
            annotationView = MKPinAnnotationView(annotation: annotation,
                                                 reuseIdentifier: identifier)
        }
        
        // Configure annotationView
        annotationView.annotation = annotation
        annotationView?.pinTintColor = .blue
        annotationView.isEnabled = true
        annotationView.animatesDrop = true
        annotationView.canShowCallout = true
        annotationView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        self.mapView.deselectAnnotation(view.annotation,
                                        animated: true)
        
        self.performSegue(withIdentifier: "CityDetailsVC",
                          sender: view.annotation)
    }
    
    // MARK: - Helper Methods
    
    /// Create all cities in Egypt manually and set their locations
    func fetchCities()
    {
        // Make sure the path is not nil - if the file is deleted for example or its name changed, path would be nil
        guard let path = Bundle.main.path(forResource: "Cities", ofType: "plist") else
        {
            print("File doesn't exists")
            return
        }
        
        // Make sure that the dictionary is created correct
        guard let cities = NSDictionary(contentsOfFile: path) else
        {
            print("File doesn't exists")
            return
        }
        
        // Get all Cities
        guard let citiesArr = cities["Cities"] as? [[String: Any]] else
        {
            print("Something wrong with parsing")
            return
        }
        
        // Create each city - without its condition - and append each one to the array
        for city in citiesArr
        {
            let location = CLLocation(latitude: city["lat"] as! Double,
                                      longitude: city["long"] as! Double)
            
            let cityObj = City(id: city["id"] as? Int,
                               name: city["name"] as? String,
                               location: location,
                               condition: nil)
            
            self.citiesArray.append(cityObj)
        }
    }
    
    /// Fetch all weather data for all cities from server - openWeatherMap -
    func requestAllWeatherDataFromServer()
    {
        // Keep tracking the number of data being fetched from server to hide progressView
        var numberOfCityData = 0
        
        for city in self.citiesArray
        {
            let parameter = ["appid": appID,
                             "id": "\(city.id!)",
                             "units": "metric"]
            
            HTTPRequest.fetchAllData(url: baseURL,
                                     parameters: parameter,
                                     successBlock: { result in
                                        
                                        if let result = result
                                        {
                                            city.condition = Condition(data: result)
                                            numberOfCityData += 1
                                        }
                                        
                                        // If all data is fetched, then hide the progressView
                                        if numberOfCityData == 27
                                        {
                                            MBProgressHUD.hide(for: self.view,
                                                               animated: true)
                                            
                                            self.addAnnotationForEachCity()
                                        }
            },
                                     failureBlock: { error in
                                        
                                        if let error = error
                                        {
                                            print("Something went wrong: \(error.localizedDescription)")
                                        }
            })
        }
    }
    
    func addAnnotationForEachCity()
    {
        for city in self.citiesArray
        {
            let annotation = CityAnnotation(coordinate: (city.location?.coordinate)!,
                                            title: city.name!,
                                            subtitle: (city.condition?.description)!)
            annotation.city = city
            
            self.mapView.addAnnotation(annotation)
        }
    }
    
    /// Configure the mapView to be centered on egypt with specific zoom level
    func setupMapView()
    {
        let center = CLLocationCoordinate2D(latitude: kEGYPTLAT,
                                            longitude: kEGYPTLONG)
        // Zooming Level
        let span = MKCoordinateSpan(latitudeDelta: 5,
                                    longitudeDelta: 10)
        
        let region = MKCoordinateRegion(center: center,
                                        span: span)
        
        self.mapView.setRegion(region,
                               animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "CitiesVC",
        let citiesVC = segue.destination as? CitiesVC
        {
            citiesVC.cities = self.citiesArray
        }
        else if segue.identifier == "CityDetailsVC",
            let cityDetailsVC = segue.destination as? CityDetailsVC
        {
            cityDetailsVC.delegate = self
            
            if let anno = sender as? CityAnnotation
            {
                cityDetailsVC.city = anno.city
            }
        }
    }
    
    // MARK: - CityDetailsVCDelegate Methods
    
    func dismissCityDetailsVC()
    {
        self.dismiss(animated: true,
                     completion: nil)
    }
}
