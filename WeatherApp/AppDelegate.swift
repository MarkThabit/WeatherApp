//
//  AppDelegate.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/24/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import UIKit

let baseURL = "http://api.openweathermap.org/data/2.5/weather?"
let appID = "a09f99cf713ffa9842d87515cfb399e6"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        return true
    }
}
