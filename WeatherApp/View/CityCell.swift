//
//  CityCell.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/25/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell
{
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var windDegree: UILabel!
    @IBOutlet weak var currentTemp: UILabel!
    
    func configureCell(city: City)
    {
        self.cityName.text = city.name
        self.humidity.text = "\((city.condition?.humidity)!)%"
        self.windSpeed.text = "\((city.condition?.windSpeed)!)"
        self.windDegree.text = "\((city.condition?.windDegree)!)"
        self.currentTemp.text = "\(Int((city.condition?.currentTemperature)!))°"
        
        self.selectionStyle = .none
    }
}
