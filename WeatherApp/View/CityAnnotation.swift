//
//  CityAnnotation.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/27/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import Foundation
import MapKit

class CityAnnotation: NSObject, MKAnnotation
{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    // Just to pass data to the next viewController, when user press the calloutAccessoryControl
    var city: City?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String)
    {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}
