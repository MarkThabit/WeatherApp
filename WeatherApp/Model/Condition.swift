//
//  Condition.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/24/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import Foundation

class Condition
{
    var currentTemperature: Double?
    var minimumTemperature: Double?
    var maximumTemperature: Double?
    var humidity: Double?
    var pressure: Double?
    var windSpeed: Double?
    var windDegree: Double?
    var description: String?
    
    init() {}
    
    init(currentTemperature: Double?,
         minimumTemperature: Double?,
         maximumTemperature: Double?,
         humidity: Double?,
         pressure: Double?,
         windSpeed: Double?,
         windDegree: Double?,
         description: String?)
    {
        self.currentTemperature = currentTemperature
        self.minimumTemperature = minimumTemperature
        self.maximumTemperature = maximumTemperature
        self.humidity = humidity
        self.pressure = pressure
        self.windSpeed = windSpeed
        self.windDegree = windDegree
        self.description = description
    }
    
    init(data: [String: Any])
    {
        if let mainSection = data["main"] as? [String: Any],
            let windSection = data["wind"] as? [String: Any],
            let weatherSection = data["weather"] as? [[String: Any]]
        {
            self.currentTemperature = mainSection["temp"] as? Double
            self.minimumTemperature = mainSection["temp_min"] as? Double
            self.maximumTemperature = mainSection["temp_max"] as? Double
            self.humidity = mainSection["humidity"] as? Double
            self.pressure = mainSection["pressure"] as? Double
            
            self.windSpeed = windSection["speed"] as? Double
            self.windDegree = windSection["deg"] as? Double
            
            self.description = weatherSection[0]["description"] as? String
        }
    }
}
