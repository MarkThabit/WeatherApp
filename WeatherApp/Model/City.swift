//
//  City.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/24/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import Foundation
import CoreLocation

class City
{
    var id: Int?  // ID is relate to openWeatherMap Database
    var name: String?
    var location: CLLocation?
    var condition: Condition?
    
    init() {}
    
    init(id: Int?,
         name: String?,
         location: CLLocation?,
         condition: Condition?)
    {
        self.id = id
        self.name = name
        self.location = location
        self.condition = condition
    }
}
