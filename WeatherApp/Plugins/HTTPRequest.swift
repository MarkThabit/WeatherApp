//
//  HTTPRequest.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/24/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import Foundation
import Alamofire

class HTTPRequest
{
    class func fetchAllData(url: String,
                            parameters: [String: Any]?,
                            successBlock: @escaping ([String: Any]?) -> Void,
                            failureBlock: @escaping (Error?) -> Void)
    {
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                failureBlock(response.error)
                                return
                            }
                            
                            guard let value = response.result.value as? [String: Any] else {
                                failureBlock(nil)
                                return
                            }
                            
                            successBlock(value)
        }
    }
}
